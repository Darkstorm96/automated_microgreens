
import board
import busio
import adafruit_ssd1306

#import draw
from PIL import Image, ImageDraw, ImageFont

i2c = busio.I2C(board.SCL, board.SDA)
oled = adafruit_ssd1306.SSD1306_I2C(128, 64, i2c)

# fill up oled with blank white
oled.fill(0)
oled.show()

# fill up oled with empty ?
oled.fill(1)
oled.show()

image = Image.new("1", (oled.width, oled.height))
draw = ImageDraw.Draw(image)
draw.rectangle((0, 0, oled.width, oled.height), outline=255, fill=255)

BORDER = 5
draw.rectangle((BORDER, BORDER, oled.width - BORDER - 1, oled.height - BORDER - 1), outline=0, fill=0)
font = ImageFont.load_default()

# For text 
text = "humidity: 77.0% \ntemperature: 29.0C"
(font_width, font_height ) = font.getsize(text)
#draw.text((oled.width//2 - font_width//2, oled.height//2 - font_height//2), text, font=font, fill=255)
draw.text(((font_width//2-oled.width//2-30), oled.height//2 - font_height//2-10), text, font=font, fill=255)
oled.image(image)
oled.show()