"""
"""
import asyncio
import random 
from sim_main import read_sensor

async def automated_SoilSensor(l, config_file=config_file):
    """
    This is an automated functions, it will run the entire work once call.
    it read lineitems from config file and read values from soil moist sensor.

    it reads list which consists collections of

    (input_dict, output_dict)

    @ arg:
        l: list of hardware, [(input_dict, output_dict),...]
        filename: str, the filename

    @ returns:
        return cell # will write the details.
        ErrorCode if error:
    """
    ## !
    await asyncio.gather(*(irricon(row, config_file) for row in l))



async def main(*args):
    await asyncio.gather(*(read_sensor(n["id"]) for n in args))

if __name__ == "__main__":

    args = [{"id":1},
            {"id":2},
            {"id":3},
            {"id":4},
            {"id":5}
            ]
    asyncio.run(main(*args))
