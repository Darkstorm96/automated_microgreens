# run relay

import RPi.GPIO as GPIO
from time import sleep

def relay(channel):
    """
    switch on / off the relay switch
    """
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(channel, GPIO.OUT)
    
    GPIO.output(channel, GPIO.LOW)
    sleep(3)
    
    GPIO.output(channel, GPIO.HIGH)
    GPIO.cleanup()
    
if __name__ == "__main__":
    relay(26)
    relay(16)