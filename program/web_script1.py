"""
This is a mini twisted script. It is suppose to provides
all info and controls within a page.

Use case :
1. Users read moisture records from web.
2. Users can check the moisture record live.
3. Users open/close valve from web live. ( Hold )
4. Users can export data from web. (Hold )

"""
from twisted.web import static
from twisted.web.static import File
from twisted.web.resource import Resource
from twisted.web.server import Site
from twisted.internet import reactor, endpoints
from jinja2 import Template
from urllib import parse

from SetPath import SetPath
import HWcaller

# Temp setup
main_page = SetPath("resource/page1.html.jinja")

class main(Resource):
    """
    This is the main site,
    """

    def __init__(self):
        """ """
        Resource.__init__(self)

    def render_GET(self, request):
        print("at render_GET()")
        print(request)
        print(request.uri)

        # this is to handle request ?
        # it is assume one request per time
        uri = request.uri.decode()
        d_uri = dict(parse.parse_qsl(parse.urlsplit(uri).query))
        print(d_uri)

        # request handler
        s = main.render_HTML(self, d_uri=d_uri)
        print(type(s))
        return s.encode()

    def render_HTML(self, d_uri):
        """
        This is to render all values into string, and substitute as html
        """
        with open(main_page, "r") as f:
            s = Template(f.read())

        if d_uri != {}:
            # assign request to relative task
            if list(d_uri.keys())[0] == "soil_moist":
                sr = main.checkSoilMoist(self)
                s = s.render(soil_moist=sr)

            elif list(d_uri.keys())[0] == "air_humid":
                sr = main.checkHumidity(self)
                s = s.render(air_humid=sr)

            elif list(d_uri.keys())[0] == "temp":
                sr = main.checkTemp(self)
                s = s.render(temp=sr)

            elif list(d_uri.keys())[0] == "led":
                status = main.ledStatus(self)
                if status == "OFF":
                    sr = main.ControlLed(self, "ON")
                    status = "ON"
                else:
                    sr = main.ControlLed(self, "OFF")
                    status = "OFF"
                s = s.render(led_status=status)

            else:
                return b"no such request"
        else:
            #sr = "Unknown"
            s = s.render(soil_moist=None)

        return s

    def checkSoilMoist(self, soil_moist_channel=1):
        """ This function is to check soil moist"""
        sm = HWcaller.I2c_SoilMoist(soil_moist_channel)
        rd = sm.read()
        print(rd)
        return rd

    def checkTemp(self, Temp_channel=21):
        """ This function is to check temperature"""
        sm = HWcaller.Humid_temp(Temp_channel)
        rd = sm.read()
        return rd["temperature"]

    def checkHumidity(self, Humid_channel=21):
        """ This function is to check air moisture"""
        sm = HWcaller.Humid_temp(Humid_channel)
        rd = sm.read()
        return rd["humidity"]

    def ControlLed(self, mode, channel=16):
        """ this is to turn on/off led """
        sm = HWcaller.relay(channel)
        if mode == "ON":
            sm.relay_on()
            return "Led ON"
        else:
            sm.relay_off()
            return "Led OFF"

    def ledStatus(self, channel=16):
        """ This is to check led status """
        sm = HWcaller.relay(channel)
        return sm.status()

    def ControlPump(self, channel, mode):
        """ this is to turn on/off pump """
        sm = HWcaller.relay(channel)
        if mode == "On":
            sm.relay_on()
        else:
            sm.relay_off()
        return None

    def ControlValve(self, mode, channel=26):
        """ this is to turn on/off valve """
        sm = HWcaller.relay(channel)
        if mode == "On":
            sm.relay_on()
        else:
            sm.relay_off()
        return None

#root = File("herbPages")
root = main()
root.putChild(b"main", main())
#root.putChild(b"main", main())
#root.putChild(b"main/herbPages/_static/dangshen.jpg", File("herbPages/_static/dangshen.jpg"))

#main.putChild(b"herbPages")

factory = Site(root)
endpoint = endpoints.TCP4ServerEndpoint(reactor, 8880)
endpoint.listen(factory)
reactor.run()
