import time
import asyncio

from datetime import datetime

import config
import SetPath
import features
import db
import pdb

import os
import schedule

from pprint import pprint

# --READ CONFIG DATA ----------------------------------------------
config_file = "test_config.yaml" # return config directory path
config_path = SetPath.SetPath("config") # config dir
def run(config_file=config_file):
    """
    run(config_file=config_file)

    This function is called by initial ,
    it will helps arrange proper data structure from
    config file for processing on features.

    It expect 1 input to 1 output itm.

    l = [(input_dict, output_dict)]

    @args:

        - config_file: str, name of the config file 

    @returns:

        None
    """

    async def main_run(l, config_file=config_file):
        """
        the main "event loop".

        @ arg:
            l: list of hardware, [(input_dict, output_dict),...]
            filename: str, the filename

        @ returns:
            return cell # will write the details.
            ErrorCode if error:
        """
        await asyncio.gather(*(irricon(row, config_file) for row in l))

    l_config = config.ReadFile(config_file)

    # This is to rearrange the bundle for features
    l_in = l_config["hardware"]["input"]
    l_out = l_config["hardware"]["output"]

    l_col = []
    for r in l_in:
            d_in = r["channel_address"]
            for _r in l_out:
                    d_out = _r["channel_address"]
                    if d_in == d_out:
                        l_col.append((r, _r))

    pprint(l_col)

    # tuple both moist sensor and valve
    # sensor is "s"; valve is "v"
    asyncio.run(main_run(l_col, config_file)) 
