#!/usr/bin/python
from gpiozero import DigitalInputDevice
import RPi.GPIO as GPIO
import spidev # to communicatewith self.spi device.
import board
import busio
import adafruit_ads1x15.ads1115 as ADS
from adafruit_ads1x15.analog_in import AnalogIn
import adafruit_ssd1306
from PIL import Image, ImageDraw, ImageFont
import Adafruit_DHT
from pprint import pprint
from statistics import mean
from datetime import datetime
from numpy import interp # To scale values
from numpy import mean
from time import sleep
import pdb

class Humid_temp():
    """
    This class read humidity and temperature from Adafruit DHT sensor.
    it should support DHT11, DHT22 sensors

    @args:
    - channel: int, GPIO number

    @returns:
    """
    def __init__(self, port, sensor=Adafruit_DHT.DHT11):
        self.port = port
        self.sensor = sensor

    def read(self):
        d = {}
        humidity, temperature = Adafruit_DHT.read_retry(self.sensor, self.port)
        d["humidity"] = humidity
        d["temperature"] = temperature
        return d

class I2c_SoilMoist():
    """
    This class handle I2c interface soil moisture sensors. This class only
    handle 1 sensor per time.

    @args:
        - channel: int, port number from ADC1115. For instance: 0, 1, 2..etc

    @returns:
    """

    def __init__(self, port, rounding=2, cal_min=0, cal_max=2.7, address=0x48):
        # create the I2C bus
        i2c = busio.I2C(board.SCL, board.SDA)
        self.ads = ADS.ADS1115(i2c, address=address)
        # port number
        d_port = {0:ADS.P0, 1:ADS.P1, 2:ADS.P2, 3:ADS.P3}
        self.port = d_port[port]
        self.rounding = rounding
        self.f = 100/cal_max - cal_min

    def read(self, repeat=5, percent=True, ret_integer=False):
        """
        """
        chan = AnalogIn(self.ads, self.port)
        lr = []
        for r in range(0, repeat):
            #s = "{:>5}\t{:>5.3f}".format(chan.value, chan.voltage)
            lr.append(chan.voltage)
            sleep(0.5)
        rslt = round(mean(lr), self.rounding)
        p_rslt = round(mean(rslt) * self.f, 1)
        return p_rslt

class Spi_SoilMoist():
    """
    This class handle SPI interface soil moisture sensors.

    @args:
        - channel: int, port number from MCP3008

    @returns:
    """

    def __init__(self, channel):
        self.cha = channel
        self.spi = spidev.SpiDev() # Created an object
        self.spi.open(0,0)

    # Read MCP3008 data
    def read(self):
        """
        This function does:
            - Connect and collect NUM READINGS sequentially from multiple soil moist sensors.

        @args:
           - self.cha, port number.

        @returns:
            - it returns [{ soil_moist port, timestamp, magnitude, error }]
        """


        def analogInput(channel):
            """
            """
            self.spi.max_speed_hz = 1350000
            adc = self.spi.xfer2([1,(8+channel)<<4,0])
            data = ((adc[1]&3) << 8) + adc[2]
            return data

        l = []
        for loop in range(0,5):
            output = analogInput(self.cha)
            output = interp(output, [0, 1023], [100, 0])
            output = int(output)
            l.append(output)
        value = mean(l)
        timestamp = datetime.today().strftime("%Y-%m-%d %H:%M")
        #d = {"port": port, "timestamp": timestamp, "value": value}
        d = {"Sensor":self.cha, "datetimes":timestamp, "readings":value}
        #print(d)

        return d["readings"]

        self.spi.close()

class Oled():
    """
    This class writes text into I2C OLED SSD1306 screen.

    @args:
    - channel: int, GPIO port number

    @returns:
    """
    def __init__(self, width=128, height=64):
        i2c = busio.I2C(board.SCL, board.SDA)
        self.oled = adafruit_ssd1306.SSD1306_I2C(width, height, i2c)

    def shows(self, text):
        """
        shows the data on the screen. ( only humidity, temperature, and soil humid itm )

        Backlog: how to handle interrupts like keyboard interrupt
        """
        # set fixed configs:
        BORDER = 5

        # Initialise image
        image = Image.new("1", (self.oled.width, self.oled.height))

        # Draw border box
        draw = ImageDraw.Draw(image)
        draw.rectangle((0, 0, self.oled.width, self.oled.height), outline=255, fill=255)
        draw.rectangle((BORDER, BORDER, self.oled.width - BORDER - 1, self.oled.height - BORDER - 1), outline=0, fill=0)

        # For text
        font = ImageFont.load_default()
        (font_width, font_height ) = font.getsize(text)
        # pls check out code bellow :
        draw.text(((font_width//2-self.oled.width//2-75), self.oled.height//2 - font_height//2-15), text, font=font, fill=255)
        self.oled.image(image)
        self.oled.show()

    def blank(self):
        """ This is to clear the screen by showing blank """
        self.oled.fill(0)
        self.oled.show()

class relay():
    """
    This class switch on/off the relay.

    @args:
    - channel : int, GPIO port numbers

    @returns:
    - None
    """

    def __init__(self, relay_channel):
        self.cha = relay_channel
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.cha, GPIO.OUT)

    def status(self):
        # To read the state
        state = GPIO.input(self.cha)
        if state:
            return 'OFF'
        else:
            return 'ON'

    def relay_on(self):
        """ switch on the intended relay"""
        GPIO.output(self.cha, GPIO.LOW)
        return 1

    def relay_off(self):
        """ switch off the intented relay """
        GPIO.output(self.cha, GPIO.HIGH)
        return 0

class Led(relay):
    """
    This class witch on/off the LED.

    @args:
    - channel: int, port number

    @returns:
    """
    def __init__(self, led_channel, relay_on=True):
        self.cha = led_channel
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.cha, GPIO.OUT)

        if relay_on is True:
            self.relay_on = relay_on
            super().__init__(led_channel)

    def on(self):
        if self.relay_on is True:
            super().relay_on()
        else:
            GPIO.output(self.cha, GPIO.HIGH)

    def off(self):
        if self.relay_on is True:
            super().relay_off()
        else:
            GPIO.output(self.cha, GPIO.LOW)

class Valve(relay):
    """
    This class witch on/off the valve

    @args:
    - channel: int, port number

    @returns:
    """
    def __init__(self, valve_channel, relay_on=True):
        self.cha = valve_channel
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.cha, GPIO.OUT)

        if relay_on is True:
            self.relay_on = relay_on
            super().__init__(valve_channel)

    def on(self):
        if self.relay_on is True:
            super().relay_on()
        else:
            GPIO.output(self.cha, GPIO.HIGH)

    def off(self):
        if self.relay_on is True:
            super().relay_off()
        else:
            GPIO.output(self.cha, GPIO.LOW)

class Pump(relay):
    """
    This class switch on/off the relay

    @args:
    - channel: int, port number

    @returns:
    """
    def __init__(self, pump_channel, relay_on=True):
        self.cha = pump_channel
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.cha, GPIO.OUT)

        if relay_on is True:
            self.relay_on = relay_on
            super().__init__(pump_channel)

    def on(self):
        if self.relay_on is True:
            super().relay_on()
        else:
            GPIO.output(self.cha, GPIO.HIGH)

    def off(self):
        if self.relay_on is True:
            super().relay_off()
        else:
            GPIO.output(self.cha, GPIO.LOW)
