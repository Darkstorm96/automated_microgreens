"""
This is a test code for callibration and adjusting some marginal errors
"""
import config

from gpiozero import DigitalInputDevice
import RPi.GPIO as GPIO
import spidev # to communicatewith self.spi device.
import board
import busio
import adafruit_ads1x15.ads1115 as ADS
from adafruit_ads1x15.analog_in import AnalogIn
import adafruit_ssd1306
from PIL import Image, ImageDraw, ImageFont
import Adafruit_DHT
from pprint import pprint
from statistics import mean
from datetime import datetime
from numpy import interp # To scale values
from numpy import mean
from time import sleep
import pdb

def test_hardware():
    """
    """
    pass

def auto_callibrate():
    """
    This function is to auto-callibrate the sensor readings
    """
    print("Pls follow instructions!")
    sleep(1)
        
    # obtain value when its full humid
    print("Measure full humid: Dip sensor entirely into water.")
    sleep(3)
    print("Start Measuring:")
    fhv = self.read()
        
    # obtain value when its zero humid
    print("Measure full humid: Dip sensor entirely into water.")
    sleep(3)
    print("Start Measuring:")
    ehv = self.read()
        
    # Set max, and min value
    # assume linear relationship
        
    return (fhv, ehv)
    
def marginal_error():
    """
    This is to report marginal error:
        - Monitoring fluactuation reading value
    """
    pass

if __name__ == __main__:
    print("test start")