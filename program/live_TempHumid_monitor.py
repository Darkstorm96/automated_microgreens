"""This is to test live monitoring humidity and temperature """
import HWcaller as hw
from time import sleep
from json import dump

ol = hw.Oled()

while True:
    d = hw.Humid_temp(21).read()
    with open("humid_temp.json", "a+") as fle:
        fd = dump(d, fle)
        #fd.update(d)
    s = f"humidity: {d['humidity']}% \ntemperature: {d['temperature']}C"
    ol.shows(s)
    sleep(30)