import os
import sys
import pdb

def SetPath(directory):
    """
    SetPath(directory)

    SetPath - set up the intend path to import witin automated_irrigation directory
    @args:
        directory: str, the name of directory

    @returns:
        p: str, the name of the set path
    """
    l = os.getcwd().split("/")[:-1]
    l.append(directory)
    #p = os.path.join(*l)
    p = "/".join(l)    
    return p
