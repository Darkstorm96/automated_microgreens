import sys
import os
from os.path import expanduser, join
from pprint import pprint
import pdb

lp = os.getcwd().split("/")
_lp = lp[:lp.index("automated_microgreens")+1]
pth = "/".join(_lp)

path = join(pth)

p = join(path, "program")
sys.path.insert(0, p)

import SetPath

def test_SetPath():
    #pdb.set_trace()
    expect = "/Users/brandontong/Documents/Agriculture/Automated_agriculture/microgreen_systems/Automated_microgreen/automated_microgreens/config/config.yaml"
    actual = SetPath.SetPath("config/config.yaml")
    assert expect == actual, f"Expect {expect} but {actual}"

if __name__ == "__main__":
    test_SetPath()
