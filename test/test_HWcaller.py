""" This is to test HWcaller.py"""

import sys
import time
from time import sleep
import os
from os.path import expanduser, join
from pprint import pprint

import SetPath

p = SetPath.SetPath("program")
sys.path.insert(0, p)

import HWcaller

#configs ------------
soil_moist_channel = 1
temp_humid_channel = 21
led_channel = 26
valve_channel = 16
pump_channel = 26


def test_Read_SPI_SoilMoisture():
    """ This is to test ReadSoilMoisture() function """
    sm = HWcaller.Spi_SoilMoist(soil_moist_channel)
    rd = sm.read()
    print(rd)
    return rd

def test_Read_I2C_SoilMoisture(soil_moist_channel):
    """ This is to test ReadSoilMoisture() function """
    sm = HWcaller.I2c_SoilMoist(soil_moist_channel)
    rd = sm.read()
    print(rd)
    return rd

def test_Read_Humid_temp(temp_humid_channel):
    """This is to test read Humid_temp() function"""
    sm = HWcaller.Humid_temp(temp_humid_channel)
    rd = sm.read()
    print(rd)
    return rd

def test_oled():
    """ this is to test oled() function"""
    sm = HWcaller.Oled()
    sm.shows("humidity: 77.0% \ntemperature: 29.0C \nsoil humid: 60%")
    sleep(10)
    sm.blank()

def test_relay():
    """ this is to test relay() function """
    sm1 = HWcaller.relay(led_channel)
    sm1.relay_on()
    sleep(2)
    sm2 = HWcaller.relay(valve_channel)
    sm2.relay_on()
    sleep(3)
    sm1.relay_off()
    sleep(2)
    sm2.relay_off()
    return "relay test code is passed"

def test_led():
    """ None """
    led = HWcaller.Led(led_channel)
    led.on()
    time.sleep(2)
    led.off()

def test_valve():
    """ None """
    valve = HWcaller.Valve(valve_channel)
    valve.on()
    time.sleep(2)
    valve.off()

def test_pump():
    """ None """
    pump = HWcaller.Pump(pump_channel)
    pump.on()
    time.sleep(2)
    pump.off()

if __name__ == "__main__":
    #print(test_ReadSoilMoisture(0))
    #print(test_ReadSoilMoisture(1))
    #test_led()
    print(test_Read_I2C_SoilMoisture(1))
    print(test_Read_I2C_SoilMoisture(0))
    test_relay()
    print(test_Read_Humid_temp(21))
    test_oled()
